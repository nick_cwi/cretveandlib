package com.cretve.andlib.users;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by cwh on 8/4/16.
 */
public class User {
    private String name;
    private String NAME = "name"; //field name for persistence storage in SharedPrefernce
    private String passwd;
    private String PASSWD = "passwd"; //field passwd for persistence storage in SharedPrefernce
    private String id; //user Id that is the searching index from server
    private String ID = "id"; //field id for persistence storage in SharedPrefernce
    //loginStatus and loginToken are temporary values identified the login session
    // without necessary to being stored in SharedPrefernce
    private boolean loginStatus;   //true = loged in; false = log off
    private String loginToken;  //Identifying the login session

    //caller context which would provide handler accessing SharedPreference
    Context mContext;

    //SharedPreference
    SharedPreferences mUserPreference;

    //persistence file for user
    private String USER_PREFER = "user_preference";

    //construction
    public User(Context context){
        mContext = context;
        mUserPreference = mContext.getSharedPreferences(USER_PREFER, 0);
    }

    //persistence throught android Preference

    //Read and Wite method for private field of this user
    public String getName() {
        if(mUserPreference != null){
            name = mUserPreference.getString(NAME, null);
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
        //persisting into SharedPerence
        if(mUserPreference != null){
            SharedPreferences.Editor editor = mUserPreference.edit();
            editor.putString(NAME, this.name);

            // Commit the edits!
            editor.commit();
        }
    }

    public String getPasswd() {
        if(mUserPreference != null){
            name = mUserPreference.getString(PASSWD, null);
        }
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
        //persisting into SharedPerence
        if(mUserPreference != null){
            SharedPreferences.Editor editor = mUserPreference.edit();
            editor.putString(PASSWD, this.passwd);

            // Commit the edits!
            editor.commit();
        }
    }

    public String getId() {
        if(mUserPreference != null){
            id = mUserPreference.getString(ID, null);
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
        //persisting into SharedPerence
        if(mUserPreference != null){
            SharedPreferences.Editor editor = mUserPreference.edit();
            editor.putString(ID, this.id);

            // Commit the edits!
            editor.commit();
        }
    }

    public boolean isLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(boolean loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }
}
