package com;

import android.app.Application;

import java.util.HashMap;

/**
 * Created by wangzy on 16/1/20.
 */
public abstract class BaseApplication<T extends BaseApplication> extends Application {


    private HashMap<String, Object> tempParamap;
    public static String tag = "tippper";

    @Override
    public void onCreate() {
        super.onCreate();
        this.tempParamap = new HashMap<String, Object>();
    }


    public void putTemPObject(String key, Object object) {
        tempParamap.put(key, object);
    }

    public boolean hasTempKey(String key) {

        return tempParamap.containsKey(key);
    }

    public Object getTempObject(String key) {

        return tempParamap.remove(key);
    }

}
