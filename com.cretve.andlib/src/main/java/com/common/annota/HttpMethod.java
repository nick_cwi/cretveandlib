package com.common.annota;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by cwh on 8/5/16.
 */
@Target(ElementType.METHOD)
public @interface HttpMethod {
    String value() default "";
}
