package com.common.annota;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by cwh on 8/5/16.
 */
@Target(ElementType.METHOD)
@Retention(RUNTIME)
public @interface Path {
    String value() default "";
}
