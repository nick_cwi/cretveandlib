package com.common.task;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.BaseApplication;
import com.common.net.NetResult;
import com.common.util.LogUtil;
import com.cretve.andlib.R;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * loginTask
 *
 * @author wangzy
 */
public class BaseTask extends AsyncTask<HashMap<String, String>, Void, NetResult> {

    NetCallBack mCallBack;
    public Dialog dialog;

    public WeakReference<Context> weakReferenceContext;

    @Deprecated
    public BaseTask(NetCallBack callBack) {
        this.mCallBack = callBack;
    }

    public BaseTask(NetCallBack callBack, Context context) {
        this.mCallBack = callBack;
        this.weakReferenceContext = new WeakReference<Context>(context);
    }


    public BaseTask( Context context,NetCallBack callBack) {
        this.mCallBack = callBack;
        this.weakReferenceContext = new WeakReference<Context>(context);
    }

    public void showDialogForSelf(boolean cancelable) {
        if (null != weakReferenceContext && null != weakReferenceContext.get()) {
            Dialog dlg = crateDialog(weakReferenceContext.get(), this, cancelable);
            if (null != dlg) {
                dlg.show();
            }
        } else {
            LogUtil.i(BaseApplication.tag, "context was gced!");
        }
    }

    public void hideDialogSelf() {

        if (null != dialog) {
            dialog.dismiss();
        }
    }


    public Dialog crateDialog(Context context, final BaseTask baseTask, final boolean cancelAble) {
        if (null == context) {
            return null;
        }

        dialog = new Dialog(context, R.style.dialog);
        View dialogView = View.inflate(context, R.layout.dialog_progress, null);
        ProgressBar mProgressBar = (ProgressBar) dialogView.findViewById(R.id.progressBarMore);
        dialog.setContentView(dialogView);
        dialog.setCancelable(cancelAble);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (null != baseTask && baseTask.getStatus() == Status.RUNNING) {
                    baseTask.cancel(true);
                }
            }
        });

        LogUtil.i(BaseApplication.tag, "create newdialog:" + dialog.hashCode());
        return dialog;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (null != mCallBack) {
            mCallBack.onPreCall();
            mCallBack.onPreCall(this);
        }
    }

    @Override
    protected NetResult doInBackground(HashMap<String, String>... params) {
        if (null != mCallBack && null!=weakReferenceContext && null!=weakReferenceContext.get()) {
            if (null == params) {
                return mCallBack.onDoInBack(null, this);
            } else {
                HashMap<String, String> paramMap = (HashMap<String, String>) params[0];
                NetResult result = mCallBack.onDoInBack(paramMap, this);
                return result;
            }
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (null != mCallBack) {
            mCallBack.onCanCell();
            mCallBack.onCanCell(this);
        }
    }

    @Override
    protected void onPostExecute(NetResult result) {
        super.onPostExecute(result);
        if (null != mCallBack && null!=weakReferenceContext  && null!=weakReferenceContext.get()) {
            mCallBack.onFinish(result);
            mCallBack.onFinish(result, this);
        }
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (null != dialog) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
