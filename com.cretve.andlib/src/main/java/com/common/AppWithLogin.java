package com.common;

import android.os.Environment;

import com.common.util.LogUtil;
import com.common.util.SharePersistent;
//import com.onesignal.OneSignal;
//import com.teacherpronto.android.activity.MainActivity;
import com.common.user.User;

import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * Created by wangzy on 16/4/12.
 */
public class AppWithLogin extends BaseApp {

//    public static final String tag = "TEACHER_PRONTO";
    public static final String TAG = "AppWithLogin";
    public static final String KEY_EMAIL = "Email";
    public static final String DirFileName = "cretve_cache";

    public static final boolean isFree = false;
    public static File CACHE_DIR_IMAGE;
    public static int RESULT_CODE_CAMERA = 1;
    public static String CACHE_DIR;
    public static String CACHE_DIR_PRODUCTS;
    public static String CACHE_DIR_EXCEL;
    public static String CACHE_DIR_PDF;

    private final String KEY_USER = "keyUser";

    private static AppWithLogin app;
    private String token;
    private int verified;
    private User user;
    private HashMap<String, Object> tempParamap;
    private boolean isMaincreate;
    private boolean isMainFront;

//    private WeakReference<MainActivity> weakreferenceMainActivity;


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        this.tempParamap = new HashMap<String, Object>();

//        initOnesignal();
    }

//    private void initOnesignal() {
//
//        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.WARN);
//
//        OneSignal.startInit(this)
//                .setAutoPromptLocation(true).setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
//            @Override
//            public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {
//                LogUtil.e(AppWithLogin.tag, "notificationOpened");
//            }
//        })
//                .init();
//
//
//    }

    public static AppWithLogin getApp() {
        return app;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getVerified() {
        return verified;
    }

    public void setVerified(int verified) {
        this.verified = verified;
    }

    public User getUser() {
        try {
            return (User) SharePersistent.getObjectValue(this, KEY_USER);
        } catch (Exception e) {
            LogUtil.e(AppWithLogin.TAG, "get user error:" + e.getLocalizedMessage());
        }
        return null;

    }

    public void setUser(User user) {
        SharePersistent.setObjectValue(this, KEY_USER, user);
    }

    public static String getTag() {
        return TAG;
    }

    public static String returnCacheDir() {
        firstRun();
        return CACHE_DIR;
    }

    public static void firstRun() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            CACHE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + DirFileName;
        } else {
            CACHE_DIR = Environment.getRootDirectory().getAbsolutePath() + "/" + DirFileName;
        }
        CACHE_DIR_PRODUCTS = CACHE_DIR + "/products";
        File cacheDir = new File(AppWithLogin.CACHE_DIR);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        File cacheDirProducts = new File(AppWithLogin.CACHE_DIR_PRODUCTS);
        if (!cacheDirProducts.exists()) {
            cacheDirProducts.mkdirs();
        }
    }

    public void onPushReceive(){

    }


//    public WeakReference<MainActivity> getWeakreferenceMainActivity() {
//        return weakreferenceMainActivity;
//    }
//
//    public void setWeakreferenceMainActivity(WeakReference<MainActivity> weakreferenceMainActivity) {
//        this.weakreferenceMainActivity = weakreferenceMainActivity;
//    }

    public void putTemPObject(String key, Object object) {
        tempParamap.put(key, object);
    }

    public boolean hasTempKey(String key) {

        return tempParamap.containsKey(key);
    }

    public void clearTempKey(String key) {
        if (tempParamap.containsKey(key)) {
            tempParamap.remove(key);
        }

    }

    public Object getTempObject(String key) {

        return tempParamap.remove(key);
    }

    public boolean isMainCreated() {

        return  isMaincreate;
    }

    public void setMaincreate(boolean maincreate) {
        isMaincreate = maincreate;
    }

    public boolean isMainFront() {

        return isMainFront;
    }
}
