package com.common.parser;

import com.common.bean.LoginBean;
import com.common.net.NetResult;
import com.common.util.LogUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class JsonParser {

	public static final String MESSAGE_OK = "OK";
	public static final String STATUS_OK = "200";
	public static final String STATUS_LOGOUT = "502";

	// 解析登陆
	public static NetResult parseLogin(InputStream ins) throws Exception, JSONException {
		String rsultJson = getJsonFromInputStream(ins);
//		LogUtil.d(App.tag, "" + rsultJson);
		JSONObject jobj = new JSONObject(rsultJson);
		String status = getStringFroJso("status", jobj);
		String message = getStringFroJso("message", jobj);
		NetResult netResult = new NetResult();
		netResult.setCode(status);
		netResult.setMessage(message);
		if (STATUS_OK.equals(status)) {
			JSONObject response = jobj.getJSONObject("responseResult");
			LoginBean lb = new LoginBean();
			String userName = getStringFroJso("username", response);
			lb.setUsername(userName);
			String accessToken = getStringFroJso("accessToken", response);
			lb.setAccessToken(accessToken);
			String createTime = getStringFroJso("createTime", response);
			lb.setCreateTime(createTime);
			String expiredTime = getStringFroJso("expiredTime", response);
			lb.setExpiredTime(expiredTime);
			String appKey = getStringFroJso("appKey", response);
			lb.setAppKey(appKey);
			String tokenStatus = getStringFroJso("tokenStatus", response);
			lb.setTokenStatus(tokenStatus);
			lb.setEmail(getStringFroJso("mail", response));
			Object[] data = { lb };
			netResult.setData(data);
		}
		return netResult;
	}

	// 解析验证邮箱
	public static NetResult parseUpdateEmail(InputStream ins) throws Exception, JSONException {
		String rsultJson = getJsonFromInputStream(ins);
//		LogUtil.d(App.tag, "" + rsultJson);
		JSONObject jobj = new JSONObject(rsultJson);
		String status = getStringFroJso("status", jobj);
		String message = getStringFroJso("message", jobj);
		NetResult netResult = new NetResult();
		netResult.setCode(status);
		netResult.setMessage(message);
		if (STATUS_OK.equals(status)) {
			String content = getStringFroJso("responseResult", jobj);
			Object[] data = { content };
			netResult.setData(data);
		}
		return netResult;
	}

	// 解析验证码
	public static NetResult parseVerifyEmail(InputStream ins) throws Exception, JSONException {
		String rsultJson = getJsonFromInputStream(ins);
//		LogUtil.d(App.tag, "" + rsultJson);
		JSONObject jobj = new JSONObject(rsultJson);
		String status = getStringFroJso("status", jobj);
		String message = getStringFroJso("message", jobj);
		NetResult netResult = new NetResult();
		netResult.setCode(status);
		netResult.setMessage(message);
		if (STATUS_OK.equals(status)) {
			JSONObject response = jobj.getJSONObject("responseResult");
			String verifyCode = getStringFroJso("verifyCode", response);
			String verifyMessage = getStringFroJso("verifyMessage", response);
			Object[] data = { verifyCode, verifyMessage };
			netResult.setData(data);
		}
		return netResult;
	}

	// 软件版本
	public static NetResult parseVersion(InputStream ins) throws Exception, JSONException {
		String rsultJson = getJsonFromInputStream(ins);
//		LogUtil.d(App.tag, "" + rsultJson);
		JSONObject jobj = new JSONObject(rsultJson);
		String status = getStringFroJso("status", jobj);
		String message = getStringFroJso("message", jobj);
		NetResult netResult = new NetResult();
		netResult.setCode(status);
		netResult.setMessage(message);
		if (STATUS_OK.equals(status)) {
			// String[] datas = { version, forceUpdate, desc, downurl,
			// updateTime, packageSize };
			JSONObject response = jobj.getJSONObject("responseResult");
			String name = getStringFroJso("name", response);
			String version = getStringFroJso("version", response);
			String url = getStringFroJso("url", response);
			String desc = getStringFroJso("desc", response);
			String updateTime = getStringFroJso("updateTime", response);
			String packageSize = getStringFroJso("packageSize", response);
			String forceUpdate = getStringFroJso("forceUpdate", response);
			Object[] data = { version, forceUpdate, desc, url, updateTime, packageSize };
			netResult.setData(data);
		}
		return netResult;
	}

	public static NetResult parseRegister(InputStream inputStream) throws JSONException {

//		JSONObject jsonObject = new JSONObject(getJsonFromInputStream(inputStream));
//		LogUtil.i(TAG, jsonObject.toString());
//
//		NetResult netResult = new NetResult();
//
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));
//
//		if (netResult.isOk()) {
//
//			JSONObject dataObj = jsonObject.getJSONObject(content_tag);
//			User user = parseUserFromJsonObject(dataObj);
//			String token = user.getToken();
//			netResult.setData(new Object[]{user});
//			App.getApp().setToken(token);
//
//		}

		return null;

	}

	public static NetResult parseProfile(InputStream inputStream) throws Exception {

//		String result = getJsonFromInputStream(inputStream);
//
//		LogUtil.i(TAG, result);
//
//		JSONObject jsonObject = new JSONObject(result);
//		NetResult netResult = new NetResult();
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));
//
//		if (netResult.isOk()) {
//
////            User user = new User();
////
//			JSONObject userObj = jsonObject.getJSONObject(content_tag);
////
////            user.setUser_id(getStringFroJso("user_id", userObj));
////            user.setEmail(getStringFroJso("email", userObj));
////            user.setFirst_name(getStringFroJso("first_name", userObj));
////            user.setLast_name(getStringFroJso("last_name", userObj));
////            user.setRegister_number(getStringFroJso("regist_number", userObj));
////            user.setPost_code(getStringFroJso("post_code", userObj));
////            user.setLatitude(getStringFroJso("lat", userObj));
////            user.setLongitude(getStringFroJso("lng", userObj));
////            user.setAvatar(getStringFroJso("avatar", userObj));
////
////            user.setPush_notification(getStringFroJso("push_notification", userObj));
////            user.setEmail_notification(getStringFroJso("email_notification", userObj));
////            user.setDistance_preference(getStringFroJso("distance_preference", userObj));
////
////            user.setForm_lodged(getStringFroJso("form_lodged", userObj));
////            user.setRegister_type(getStringFroJso("register_type", userObj));
////            user.setRegister_state(getStringFroJso("register_state", userObj));
////
////
////            user.setPhone(getStringFroJso("phone", userObj));
////            user.setTeaching_preference(getStringFroJso("teaching_preference", userObj));
////
////
////            JSONArray teachSubjectsArray = userObj.getJSONArray("subjects_preference");
////
////
////            if (null != teachSubjectsArray && teachSubjectsArray.length() > 0) {
////
////                for (int i = 0, isize = teachSubjectsArray.length(); i < isize; i++) {
////                    user.addSubjectsPreference(teachSubjectsArray.getString(i));
////                }
////            }
////
////
////            JSONArray levelexpercesArray = userObj.getJSONArray("level_experience");
////
////            if (null != levelexpercesArray && levelexpercesArray.length() > 0) {
////                for (int i = 0, isize = levelexpercesArray.length(); i < isize; i++) {
////                    user.addLevelExperences(levelexpercesArray.getString(i));
////                }
////            }
////
////            user.setYear_experience(getStringFroJso("year_experience", userObj));
////            user.setIndroduction(getStringFroJso("introduction", userObj));
////            user.setVerify(getStringFroJso("verified", userObj));
//
//			netResult.setData(new Object[]{parseUserFromJsonObject(userObj)});
//
//			App.getApp().setUser((User) netResult.getData()[0]);
//		}

		return null;

	}

	public static NetResult parseVerification(InputStream inputStream) throws JSONException {
//		String result = getJsonFromInputStream(inputStream);
//
//
//		LogUtil.e(App.tag, "verify:" + result);
//
//		LogUtil.i(TAG, result);
//
//		JSONObject jsonObject = new JSONObject(result);
//		NetResult netResult = new NetResult();
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));

		return null;
	}

	public static NetResult parseUpload(String json) throws JSONException {

//		LogUtil.i(TAG, "result upload json:" + json);
//
//		JSONObject jsonObject = new JSONObject(json);
//		NetResult netResult = new NetResult();
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));
//
//		if (netResult.isOk()) {
//
//			JSONObject dataObj = jsonObject.getJSONObject(content_tag);
//
//			netResult.setData(new Object[]{dataObj.getString("avatar")});
//
//		}

		return null;
	}

	public static NetResult parseAvailability(InputStream inputStream) throws JSONException {
//		String result = getJsonFromInputStream(inputStream);
//		LogUtil.i(TAG, result);
//
//		JSONObject jsonObject = new JSONObject(result);
//		NetResult netResult = new NetResult();
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));
//
//		if (netResult.isOk()) {
//
//			JSONArray array = jsonObject.getJSONArray(content_tag);
//
//			String format = "";
//			String year, month, day;
//			ArrayList<Availability> avail = new ArrayList<>();
//
//			for (int i = 0; i < array.length(); i++) {
//				Availability availability = new Availability();
//
//				availability.setId(getStringFroJso("id", array.getJSONObject(i)));
//				availability.setUser_id(getStringFroJso("user_id", array.getJSONObject(i)));
//
//				format = getStringFroJso("date", array.getJSONObject(i));
//				year = format.substring(0, 4);
//				month = format.substring(format.indexOf("-") + 1, format.lastIndexOf("-"));
//				day = format.substring(format.lastIndexOf("-") + 1);
//
//				if (month.substring(0, 1).equals("0")) {
//					month = month.substring(1);
//				}
//
//				if (day.substring(0, 1).equals("0")) {
//					day = day.substring(1);
//				}
//
//				availability.setYear(year);
//				availability.setMonth(month);
//				availability.setDay(day);
//
//				availability.setAvailability(getStringFroJso("availability", array.getJSONObject(i)));
//				availability.setBooking(getStringFroJso("booking", array.getJSONObject(i)));
//				avail.add(availability);
//
//			}
//			netResult.setData(new Object[]{avail});
//		}

		return null;
	}

	public static NetResult parseAvailabilityResponse(InputStream inputStream) throws JSONException {
//		String result = getJsonFromInputStream(inputStream);
//		LogUtil.i(TAG, result);
//
//		JSONObject jsonObject = new JSONObject(result);
//		NetResult netResult = new NetResult();
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));
//
//		if (netResult.isOk()) {
//
//			JSONObject obj = jsonObject.getJSONObject(content_tag);
//
//			JSONArray array = obj.getJSONArray("dates");
//
//			LogUtil.i("AvailResponse", getStringFroJso("user_id", obj) + " " + getStringFroJso("date", obj)
//					+ " " + getStringFroJso("availability", obj) + " " + array.toString() + " " + getStringFroJso("id", obj));
//
//		}

		return null;
	}

	public static NetResult parseUpdateSetting(InputStream inputStream) throws JSONException {
//		String result = getJsonFromInputStream(inputStream);
//
//		LogUtil.i(TAG, result);
//
//		JSONObject jsonObject = new JSONObject(result);
//		NetResult netResult = new NetResult();
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));

		return null;
	}

	public static NetResult parseJobList(InputStream inputStream) throws JSONException {

//		JSONObject jsonObject = new JSONObject(getJsonFromInputStream(inputStream));
//		LogUtil.i(TAG, jsonObject.toString());
//
//		NetResult netResult = new NetResult();
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));
//
//		ArrayList<Job> jobsList = new ArrayList<>();
//		int totals = 0;
//
//		if (netResult.isOk()) {
//
//			try {
//
//				totals = jsonObject.getJSONObject(content_tag).getInt("total");
//
//
//				JSONArray jobs = jsonObject.getJSONObject(content_tag).getJSONArray("jobs");
//				if (null != jobs && jobs.length() > 0) {
//
//					for (int i = 0, isize = jobs.length(); i < isize; i++) {
//
//						JSONObject jobObject = jobs.getJSONObject(i);
//
//						Job job = new Job();
//
//						job.setJobId(getStringFroJso("job_id", jobObject));
//						job.setType(getStringFroJso("type", jobObject));
//						job.setTypeDescription(getStringFroJso("type_description", jobObject));
//						job.setStartDate(getStringFroJso("start_date", jobObject));
//
//						job.setEndDate(getStringFroJso("end_date", jobObject));
//						job.setStartTime(getStringFroJso("start_time", jobObject));
//						job.setEndTime(getStringFroJso("end_time", jobObject));
//						job.setTeacherType(getStringFroJso("teacher_type", jobObject));
//
//
//						JSONArray teachingLevels = jobObject.getJSONArray("teaching_level");
//						if (null != teachingLevels && teachingLevels.length() > 0) {
//							for (int x = 0, xisize = teachingLevels.length(); x < xisize; x++) {
//								job.addTeahingLeve(teachingLevels.getString(x));
//							}
//						}
//
//
//						job.setJobTitle(getStringFroJso("job_title", jobObject));
//						job.setJobDescription(getStringFroJso("job_description", jobObject));
//
//
//						try {
//							job.setOffer_end_time(jobObject.getLong("offer_end_time"));
//						} catch (Exception e) {
//							LogUtil.e(App.tag, "parse offer end time error");
//							job.setOffer_end_time(-1);
//						}
//
//
//						job.setCanced(getStringFroJso("cancelled", jobObject));
//						job.setExpired(getStringFroJso("expired", jobObject));
//
//
//						JSONObject schoolObj = jobObject.getJSONObject("school");
//
//						School school = new School();
//
//						school.setSchoolName(getStringFroJso("school_name", schoolObj));
//						school.setSchoolType(getStringFroJso("school_type", schoolObj));
//						school.setContactName(getStringFroJso("contact_name", schoolObj));
//						school.setContactEmail(getStringFroJso("contact_email", schoolObj));
//						school.setContactPhone(getStringFroJso("contact_phone", schoolObj));
//
//						school.setAddressTreet(getStringFroJso("address_street", schoolObj));
//						school.setAddressSuburb(getStringFroJso("address_suburb", schoolObj));
//						school.setAddressState(getStringFroJso("address_state", schoolObj));
//						school.setAddressPostCode(getStringFroJso("address_postcode", schoolObj));
//
//						school.setCurrentEnrolments(getStringFroJso("current_enrolments", schoolObj));
//						school.setLat(getStringFroJso("lat", schoolObj));
//						school.setLng(getStringFroJso("lng", schoolObj));
//						school.setLog(getStringFroJso("logo", schoolObj));
//
//						job.setSchool(school);
//
//						jobsList.add(job);
//
//					}
//
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//				LogUtil.i(App.tag, "parse job list error:" + e.getLocalizedMessage());
//			}
//
//		}
//
//		netResult.setData(new Object[]{jobsList, totals});

		return null;

	}

	public static NetResult parseCommonSimple(InputStream inputStream) throws JSONException {

//		JSONObject jsonObject = new JSONObject(getJsonFromInputStream(inputStream));
//		LogUtil.i(TAG, jsonObject.toString());
//
//		NetResult netResult = new NetResult();
//
//		netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
//		netResult.setCode(getStringFroJso(status_tag, jsonObject));
//		netResult.setErrors(getStringFroJso(error_tag, jsonObject));


		return null;

	}

	// =====公共方法======================
	private static String getJsonFromInputStream(InputStream ins) {
		if (null != ins) {
			StringBuffer sbf = new StringBuffer();
			BufferedInputStream bfris = new BufferedInputStream(ins);
			byte[] buffer = new byte[512];
			int ret = -1;
			try {
				while ((ret = bfris.read(buffer)) != -1) {
					sbf.append(new String(buffer, 0, ret));
				}
				bfris.close();
				ins.close();
				return sbf.toString();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static String getStringFroJso(String key, JSONObject jobj) throws JSONException {
		if (jobj.has(key)) {
			return jobj.getString(key);
		} else {
			return "";
		}

	}
}
