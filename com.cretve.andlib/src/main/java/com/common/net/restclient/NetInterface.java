package com.common.net.restclient;

import android.content.Context;

import com.common.AppWithLogin;
import com.common.annota.HttpMethod;
import com.common.annota.Path;
import com.common.net.FormFile;
import com.common.net.HttpRequester;
import com.common.net.NetResult;
import com.common.parser.JsonParser;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by clia on 2016/5/23.
 */
public class NetInterface {
    private static AppWithLogin app;
    private String path;
    private String method;
    JsonParser jsonParser;

    public NetInterface(AppWithLogin app, String addr, JsonParser jsonPaser){
        this.app = app;
        this.base_path = addr;
        this.jsonParser = jsonPaser;
    }

    //Initiate service, mainly setting server's addr
    public NetInterface getRestClient(String path, String method){
        this.path = path;
        this.method = method;

        return this;
    }

    private static String base_path = ""; // testing server

    public NetResult register(Context context, JSONObject customizedObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, customizedObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseRegister(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult login(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = null;//JSONHelper.parseLogin(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult getProfile(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseProfile(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult getVerify(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseVerification(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult verify(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseVerification(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult uploadAvatar(FormFile[] formFiles, HashMap<String, String> map) throws Exception {
        String json = HttpRequester.postUpload(base_path + path, map, formFiles, app.getApp().getToken());
        if (null != json) {
            NetResult netResult = JsonParser.parseUpload(json);
            return netResult;
        }
        return null;
    }

    public NetResult uploadProfile(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseProfile(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult getAvailability(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseAvailability(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult setAvailability(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseAvailabilityResponse(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult updateSetting(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseUpdateSetting(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public NetResult getJobList(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseJobList(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public NetResult acetDenyJob(Context context, JSONObject jsonObject) throws Exception {
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, method);
        if (null != jsonInputStream) {
            NetResult netResult = JsonParser.parseCommonSimple(jsonInputStream);
            return netResult;
        }
        return null;
    }


    //=====================common method======================


    private InputStream createCallBackInputStream(Context context, JSONObject customizedObject, String path, String requestType) throws Exception {


        JSONObject commonObject = new JSONObject();

        commonObject.put("app_version", Tool.getVersionName(context));
        commonObject.put("client_type", "0");
        commonObject.put("deviceId", Tool.getImei(context));

        customizedObject.put("common", commonObject);


        String jsonRequest = customizedObject.toString();


        //=========================

        String token = null;
        if (null != app.getApp().getUser() && !StringUtils.isEmpty(app.getApp().getToken())) {
            token = app.getApp().getToken();
        }


        LogUtil.i(app.TAG, "path:" + path);
        LogUtil.i(app.TAG, "" + jsonRequest);
        LogUtil.i(app.TAG, "======token=======");
        LogUtil.e(app.TAG, "" + token);
        LogUtil.i(app.TAG, "======token end=======");
        InputStream jsonInputStream = HttpRequester.doHttpRequestText(context, path, jsonRequest, token, requestType);
        return jsonInputStream;
    }

    /**
     * create request Object from map to jsonobject
     *
     * @param paramMap
     * @return
     * @throws JSONException
     */
    public JSONObject buildCustomizedObject(HashMap<String, String> paramMap) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            jsonObject.put(entry.getKey(), entry.getValue());
        }

        return jsonObject;
    }

//    private static JSONObject parseAnnota(String methodName){
//        Method method;
//        JSONObject result = new JSONObject();
//        try {
//            method = NetInterface.class.getMethod(methodName);
//            if(method.isAnnotationPresent(Path.class)){
//                String path = method.getAnnotation(Path.class).value();
//
//                result.put("path", (path == null? path:""));
//            }
//            if(method.isAnnotationPresent(HttpMethod.class)){
//                String httpMethod = method.getAnnotation(HttpMethod.class).value();
//                result.put("method", (httpMethod == null? httpMethod:""));
//            }
//        }catch (NoSuchMethodException e){
//
//        }catch (JSONException e){
//
//        }
//
//        return result;
//    }
}
